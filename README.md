[![](https://framasoft.org/nav/img/icons/ati/sites/git.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

Framatalk est un service en ligne de visio-conférence que [Framasoft](https://framasoft.org) propose sur le site : <https://framatalk.org>.

Il repose sur le logiciel [Jitsi Meet](https://jitsi.org/Projects/JitsiMeet) que nous n'avons pas modifié.

Si vous souhaitez traduire la page d’accueil, allez sur <https://weblate.framasoft.org/projects/framatalk/homepage/>.
* * *

Framatalk is an visio-conference online service provided by [Framasoft](https://framasoft.org) on <https://framatalk.org>.

It’s based on [Jitsi Meet](https://jitsi.org/Projects/JitsiMeet) that we didn’t modify.

If you want to translate the homepage, go on <https://weblate.framasoft.org/projects/framatalk/homepage/>.
